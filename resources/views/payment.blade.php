<!DOCTYPE html>
<html lang="en">

<head>
    <title>Payment Gateway Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>

    <div class="container mt-4">
        <center><h2>Payment Form</h2></center>
        <form action="{{ route('pay') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="amount">Amount</label>
                <input type="number" class="form-control" id="email" placeholder="Enter amount to pay" name="amount" required value="100">
            </div>
            <button type="submit" name="gateway" value="mollie" class="btn btn-primary btn-block">Pay with Mollie</button>
            <button type="submit" name="gateway" value="paytm" class="btn btn-primary btn-block">Pay with PayTM</button>
            {{-- <button type="submit" name="gateway" value="instamojo" class="btn btn-primary btn-block">Pay with Instamojo</button> --}}
            <button type="submit" name="gateway" value="flutterwave" class="btn btn-primary btn-block">Pay with Flutterwave</button>
        </form>
    </div>

</body>

</html>