<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index']);

Route::post('pay', [PaymentController::class, 'pay'])->name('pay');

Route::get('success', [PaymentController::class, 'success'])->name('success');

Route::get('fail', [PaymentController::class, 'fail'])->name('fail');

Route::get('payment/mollie/webhooks', [PaymentController::class, 'mollieHandleWebhookNotification'])->name('payment.mollie.webhooks');

Route::post('callback', [PaymentController::class, 'paytmCallback'])->name('paytm_callback');

Route::get('callback', [PaymentController::class, 'flutterwaveCallback'])->name('flutterwave_callback');
