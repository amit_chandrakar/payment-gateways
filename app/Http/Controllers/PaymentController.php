<?php

namespace App\Http\Controllers;

use Anand\LaravelPaytmWallet\Facades\PaytmWallet;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Instamojo\Instamojo;
use Mollie\Laravel\Facades\Mollie;
use KingFlamez\Rave\Facades\Rave as Flutterwave;

class PaymentController extends Controller
{

   public function pay(Request $request)
    {
        switch ($request->gateway) {
            case 'mollie':
                return $this->molliePreparePayment();
                break;
            case 'instamojo':
                return $this->instamojoPreparePayment();
                break;
            case 'paytm':
                return $this->paytmPreparePayment();
                break;
            case 'flutterwave':
                return $this->flutterwavePreparePayment();
                break;
            default:
                # code...
                break;
        }
    }

    /* public function instamojoPreparePayment()
    {
        $authType = "app";

        $api = new Instamojo(
            config('services.instamojo.api_key'),
            config('services.instamojo.auth_token'),
            config('services.instamojo.url')
        );

        try {
            $response = $api->createPaymentRequest(array(
                "purpose" => "FIFA 16",
                "amount" => "3499",
                "send_email" => true,
                "email" => "foo@example.com",
                "redirect_url" => route('success'),
                ));
        }
        catch (Exception $e) {
            print('Error: ' . $e->getMessage());
        }
    } */

    public function molliePreparePayment()
    {
        $payment = Mollie::api()->payments->create([
            "amount" => [
                "currency" => "EUR",
                "value" => "10.00" // You must send the correct number of decimals, thus we enforce the use of strings
            ],
            "description" => "Order #12345",
            "redirectUrl" => route('success'),
            "webhookUrl" => route('payment.mollie.webhooks'),
            "metadata" => [
                "order_id" => "12345",
                "name" => "amit",
            ],
        ]);

        Session::put('key', $payment->id);

        // redirect customer to Mollie checkout page
        return redirect($payment->getCheckoutUrl(), 303);
    }
  
    public function mollieHandleWebhookNotification(Request $request) 
    {
        $paymentId = $request->input('id');
        $payment = Mollie::api()->payments->get($paymentId);

        if ($payment->isPaid())
        {
            echo 'Payment received.';
            // Do your thing ...
        }
    }
    
    public function success() {
        $paymentId = Session::get('key');
        $payment = Mollie::api()->payments->get($paymentId);

        $paymentId = $payment->id;
        $amount = $payment->amount->value;
        $currency = $payment->amount->currency;
        $orderId = $payment->metadata->order_id;
        $status = $payment->status;

        return view('success', compact('paymentId', 'amount', 'currency', 'orderId', 'status'));
    }
    
    public function paytmPreparePayment()
    {
        $payment = PaytmWallet::with('receive');

        $payment->prepare([
            'order' => 'ORDER@'.rand(1111,9999), 
            'user' => rand(1111,9999),
            'mobile_number' => '88398'.rand(11111,99999),
            'email' => 'amit@froiden.com',
            'amount' => rand(1,99),
            'callback_url' => route('paytm_callback')
        ]);

        return $payment->receive();
    }

    public function paytmCallback()
    {
        $transaction = PaytmWallet::with('receive');

        $response = $transaction->response();

        $paymentId = $response['TXNID'];
        $amount = $response['TXNAMOUNT'];
        $currency = $response['CURRENCY'];
        $orderId = $response['ORDERID'];
        $status = $response['STATUS'];

        return view('success', compact('paymentId', 'amount', 'currency', 'orderId', 'status'));
    }
    
    public function flutterwavePreparePayment()
    {
        //This generates a payment reference
        $reference = Flutterwave::generateReference();

        // Enter the details of the payment
        $data = [
            'payment_options' => 'card,banktransfer',
            'amount' => 500,
            'email' => 'amit@froiden.com',
            'tx_ref' => $reference,
            'currency' => "NGN",
            'redirect_url' => route('flutterwave_callback'),
            'customer' => [
                'email' => 'amit@gmail.com',
                "phone_number" => '9087654321',
                "name" => 'Amit'
            ],
            "customizations" => [
                "title" => 'Movie Ticket',
                "description" => "20th October"
            ]
        ];

        $payment = Flutterwave::initializePayment($data);


        if ($payment['status'] !== 'success') {
            // notify something went wrong
            return;
        }

        return redirect($payment['data']['link']);
    }

     public function flutterwaveCallback()
    {
        $status = request()->status;

        //if payment is successful
        if ($status ==  'successful') {
        
            $transactionID = Flutterwave::getTransactionIDFromCallback();
            $data = Flutterwave::verifyTransaction($transactionID);

            $paymentId = $data['data']['tx_ref'];
            $amount = $data['data']['amount'];
            $currency = $data['data']['currency'];
            $orderId = $data['data']['id'];
            $status = $data['status'];

            return view('success', compact('paymentId', 'amount', 'currency', 'orderId', 'status'));
        }
        elseif ($status ==  'cancelled'){
            //Put desired action/code after transaction has been cancelled here
        }
        else{
            //Put desired action/code after transaction has failed here
        }

    }

}